{
    "name": "Sequence Preview",
    "version": "2.0.1.0.0",
    "category": "Localization",
    "website": "https://gitlab.com/flectra-community/l10n-thailand",
    "author": "Sansiri Tanachutiwat, Odoo Community Association (OCA)",
    "depends": ["base", "l10n_th_sequence_refactored"],
    "data": ["views/ir_sequence_view.xml"],
    "license": "AGPL-3",
    "installable": True,
}
