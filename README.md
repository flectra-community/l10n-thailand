# Flectra Community / l10n-thailand

None



Available addons
----------------

addon | version | summary
--- | --- | ---
[l10n_th_sequence_refactored](l10n_th_sequence_refactored/) | 2.0.1.0.1| Base Sequence
[l10n_th_withholding_tax_cert](l10n_th_withholding_tax_cert/) | 2.0.1.0.1| Thai Localization - Withholding Tax Certificate
[l10n_th_sequence_range_end](l10n_th_sequence_range_end/) | 2.0.1.0.0| Sequence - Range End
[l10n_th_base_location](l10n_th_base_location/) | 2.0.1.0.0| Thai Localization - Base Location
[l10n_th_expense_withholding_tax](l10n_th_expense_withholding_tax/) | 2.0.1.0.0| Thai Localization - Expense Withholding Tax
[l10n_th_fonts](l10n_th_fonts/) | 2.0.1.0.2| Collection of all fonts thai
[l10n_th_tax_report](l10n_th_tax_report/) | 2.0.1.0.0| Thailand Localization - TAX Reports
[l10n_th_withholding_tax_cert_form](l10n_th_withholding_tax_cert_form/) | 2.0.1.0.1| Thai Localization - Withholding Tax Certificate Form
[l10n_th_amount_to_text](l10n_th_amount_to_text/) | 2.0.1.0.0| Convert Amount Text to Thai
[l10n_th_expense_tax_invoice](l10n_th_expense_tax_invoice/) | 2.0.1.0.0| Thai Localization - Expense Tax Invoice
[l10n_th_promptpay](l10n_th_promptpay/) | 2.0.1.0.0| Use PromptPay QR code with transfer acquirer.
[l10n_th_sequence_preview](l10n_th_sequence_preview/) | 2.0.1.0.0| Sequence Preview
[l10n_th_sequence_be](l10n_th_sequence_be/) | 2.0.1.0.1| Thai Localization - Sequence with Buddhist Era Year
[l10n_th_tax_invoice](l10n_th_tax_invoice/) | 2.0.1.0.3| Thai Localization - Account Tax Invoice
[l10n_th_sequence_qoy](l10n_th_sequence_qoy/) | 2.0.1.0.1| Thai Localization - Sequence with Quarter
[l10n_th_withholding_tax](l10n_th_withholding_tax/) | 2.0.1.0.3| Thai Localization - Withholding Tax
[l10n_th_withholding_tax_report](l10n_th_withholding_tax_report/) | 2.0.1.0.0| Thailand Localization - Withholding Tax Report
[l10n_th_sequence_branch](l10n_th_sequence_branch/) | 2.0.1.0.0| Thai Localization - Sequence with Company Branch
[l10n_th_partner](l10n_th_partner/) | 2.0.2.1.0| Thai Localization - Partner
[l10n_th_company_novat](l10n_th_company_novat/) | 2.0.1.1.0| Thai Localization - Comapny/Partner, VAT/NOVAT setup


