# Copyright 2020 Ecosoft Co., Ltd. (http://ecosoft.co.th)
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).

{
    "name": "Thai Localization - Convert Amount Text to Thai",
    "version": "2.0.1.0.0",
    "author": "Ecosoft, Odoo Community Association (OCA)",
    "website": "https://gitlab.com/flectra-community/l10n-thailand",
    "license": "AGPL-3",
    "category": "Localization",
    "summary": "Convert Amount Text to Thai",
    "depends": ["base"],
    "external_dependencies": {"python": ["num2words"]},
    "data": [],
    "installable": True,
    "development_status": "Alpha",
    "maintainers": ["Saran440"],
}
